import { Router, Request as ExpressRequest, Response, NextFunction } from 'express';
import * as Request from 'request'; 

export class StockRoute {
  router: Router;
  constructor() {
    this.router = Router();
    this.init();
  }

  private init() {
    this.router.get('/get/:symbol', this.getStock.bind(this));
    this.router.get('/update/:symbol', this.updateStock.bind(this));
  }

  public getStock(req: ExpressRequest, res: Response, next: NextFunction) {
    const symbol: string = req.params.symbol;

    if(!this.isValid(symbol)){
      res.send('Invalid Input');
    } else {
      Request.get(`https://api.iextrading.com/1.0/stock/${symbol}/chart/1d`, (err, response, body) => {
        res.send(response.body);
      });
    }
  }

  public updateStock(req: ExpressRequest, res: Response, next: NextFunction) {
    const symbol: string = req.params.symbol;

    if(!this.isValid(symbol)) {
      res.send('Invalid Input');
    } else {
      Request.get(`https://api.iextrading.com/1.0/stock/${symbol}/quote`, (err, response, body) => {
        res.send(response.body);
      });
    }
  }

  private isValid(input: string): boolean {
    if (!input) {
      return false;
    }
    return /^[0-9a-zA-Z]+$/.test(input)
  }
}
