import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as errorhandler from 'errorhandler';
import { Server as HttpServer } from 'http';
import { StockRoute } from './routes/stock-route'; 

export class Server {
  public app: express.Application;
  public instance: HttpServer;

  public static bootstrap(port: number, host: string): Promise<Server> {
    return new Promise<Server>((resolve, reject) => {
      let _server = new Server();

      // Make sure server is listening for connections
      _server.instance = _server.app.listen(port, host, () => {
        if(_server.instance.listening) {
          resolve(_server);
        } else {
          reject();
        }
      });
    })
  }

  private constructor () {
    // Create express application
    this.app = express();

    // Configure application
    this.config();

    // Add routes
    this.routes();
  }

  private config() {
    // Add any static paths
    this.app.use(express.static(path.join(__dirname, '../client')));

    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));

    if (process.env.NODE_ENV === 'development') {
      this.app.use(errorhandler());
    }
  }

  private routes() {
    this.app.use('/api/v1/stocks', new StockRoute().router);
  }
}

process.env.NODE_ENV = 'development';
Server.bootstrap(3000, 'localhost').then(() => {
  console.log('Listening at http://localhost:3000/');
}).catch(() => {
  console.error('Failed bootstrapping the server');
});
