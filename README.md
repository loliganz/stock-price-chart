# Stock Price Client

This application displays a single companies stock data over the past: 1 day, 5 days, 1 month, 3 months, and 6 months.

## Getting started: 👍
These applications need to be in your PATH: node, npm, gulp, ng, and tsc

Open a command prompt/terminal and change directory into the project root. 

```bash
# Install packages
npm install

# Build the frontend client
cd client/
ng build

# Build the server
cd ../server/
tsc

# Launch the client/server
cd ../
gulp start
```