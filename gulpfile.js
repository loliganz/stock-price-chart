const spawn = require('child_process').spawnSync;
const gulp = require('gulp');
const del = require('del');
const rootDir = process.cwd();

gulp.task('build-client', () => {
    del.sync(`${rootDir}/dist/client`);
    return spawn('ng', ['build'], {stdio: 'inherit', cwd: `${rootDir}/client`});
});

gulp.task('build-server', () => {
    del.sync(`${rootDir}/dist/server`);
    return spawn('tsc', {stdio: 'inherit', cwd: `${rootDir}/server`})
});

gulp.task('build', () => {
    return gulp.start('build-client', 'build-server');
});

gulp.task('start', () => {
    return spawn('node', ['server.js'], {stdio: 'inherit', cwd: `${rootDir}/dist/server`});
});