import { Component, ViewChild, OnInit } from '@angular/core';
import { ChartService } from './chart.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  private stockTimer: any;
  public stockSymbol: string;
  public stockData: Array<any> = [
    {data: [], label: ''}
  ];
  public stockLabels: Array<any> = [];
  public stockOptions = {
    responsive: true,
    animation: {
      duration: 0
    }
  }
  public stockChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  
  constructor(private chartService: ChartService) {}
  public ngOnInit() {
    this.getStock('MSFT');
  }

  /**
   * Retrieves data points for a single stock over the last day.
   * @param symbol The stock symbol to lookup
   */
  public async getStock(symbol: string) {
    clearTimeout(this.stockTimer);
    const datapoints = await this.chartService.getStock(symbol)
    let data: Array<number> = [];
    let labels: Array<string> = [];

    for (const point of datapoints) {
      if (!point) {
        continue;
      }
      labels.push(point.label);
      data.push(point.marketAverage);
    }
    this.stockSymbol = symbol;
    this.stockData[0] = {data: [...data], label: symbol};
    this.stockLabels = [...labels];
    this.stockTimer = setInterval(() => this.updateStock(), 60000);
  }

  public async updateStock() {
    const datapoint = await this.chartService.updateStock(this.stockSymbol);
    this.stockData[0].data.push(datapoint.latestPrice);

    const label = this.parseTime(datapoint.latestUpdate);
    this.stockLabels = [...this.stockLabels, label];
  }

  private parseTime(time: number) {
    let d = new Date(0);
    d.setUTCMilliseconds(time);

    const ESTOffset = 3;
    const hours = (d.getHours() + ESTOffset) % 12;
    const min = d.getMinutes();

    const strHours = (hours == 0) ? '12' : hours;
    const strMin = min < 10 ? `0${min}` : min
    const ampm = hours >= 12 ? 'PM' : 'AM';
    
    return `${strHours}:${strMin} ${ampm}`;
  }
}