import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { data } from './data';

@Injectable()
export class ChartService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getStock(symbol: string) {
    return this.httpClient.get<any>(`api/v1/stocks/get/${symbol}`)
      .toPromise().catch((err) => {console.error(err)});
  }
  updateStock(symbol: string) {
    return this.httpClient.get<any>(`api/v1/stocks/update/${symbol}`)
      .toPromise().catch((err) => {console.error(err)});
  }
}
